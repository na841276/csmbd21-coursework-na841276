from functools import reduce
import multiprocessing as mp


class Job:
    def __init__(self, data_file_path):
        self.data_file_path = data_file_path

    def get_map_in(self, x):
        """ Map data files into key value pairs"""
        map_list = []
        # loop to iterate through file and map
        for record in open(self.data_file_path):
            # stripping if there is any white space
            record = record.strip()
            # splitting data from the file by comma
            record = record.split(',')
            # Retreiving the required record
            result = record[x]
            # appending result
            map_list.append(result)
        return map_list

    def map_func(self, x):
        """ Occurence mapper"""
        return x, 1

    def shuffle(self, mapper_out):
        """ Organise the mapped key value to generate list of occurances"""
        data_dict = {}
        # create a key if doesnt exist
        for k, v in mapper_out:
            if k not in data_dict:
                data_dict[k] = [v]
            # Append the occurences value id key already exist
            else:
                data_dict[k].append(v)
        return data_dict

    def reducer(self, x, y):
        "Sum reducer"
        return x + y

    def reduce_func(self, items):
        " Get key values from items and return reduced value"
        key = items[0]
        values = items[1]
        reduced_value = reduce(self.reducer, values)
        return key, reduced_value

    def run(self):
        " Run the process"
        with mp.Pool(processes=mp.cpu_count()) as pool:
            # convert records to list
            map_in = self.get_map_in(0)
            # calculate chunk size based on cpu
            chunk_size = int(len(map_in) / mp.cpu_count())
            # map list to get key value pairs
            map_out = pool.map(self.map_func, map_in, chunksize=chunk_size)
            # input map_out to shuffler to get consolidated key value pairs
            reduce_in = self.shuffle(map_out)
            # calculate sum of values
            reduce_out = pool.map(self.reduce_func, reduce_in.items(), chunksize=chunk_size)
            # Sorting reduce_out values
            reduce_out.sort(key=lambda x: x[1], reverse=True)
            print(f"The passenger id who travelled the most is  {reduce_out[0][0]} with {reduce_out[0][1]} number of trips")
            with open('output.txt', 'w') as f:
                f.write(f'The passenger id who travelled the most is  {reduce_out[0][0]} with {reduce_out[0][1]} number of trips \n\n')
                f.write(f'Other passengers with highest number of flights in descending order\n\n')
                for record in reduce_out:
                    f.write(f'{record}\n')


if __name__ == '__main__':
    data_file_path = 'data/AComp_Passenger_data_no_error.csv'
    map_reducer = Job(data_file_path)
    map_reducer.run()
